Align = {
    LEFT: 'left',
    RIGHT: 'right',
    TOP: 'top',
    BOTTOM: 'bottom',
    CENTER: 'center'
}

function Text(
    content,
    rotated,
    hAlign,
    vAlign,
    size,
    xShift,
    yShift
) {
    this.content = content;
    this.rotated = rotated;
    this.align = {
        h: hAlign,
        v: vAlign
    };
    this.size = size;
    this.shift = {
        x: xShift,
        y: yShift
    };
}

EmptyText = new Text(
    '',
    false,
    Align.LEFT,
    Align.TOP,
    10,
    0,
    0
);
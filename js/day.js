function Day(
    color,
    weekday,
    month,
    monthDay,
    labels,
) {
    this.color = color;
    this.weekday = weekday;
    this.month = month;
    this.monthDay = monthDay;
    this.labels = labels;
}
function PdfCalendar(
    tableCalendar,
    title
) {
    function HorisontalLine(
        x,
        y,
        length,
        thickness
    ) {
        this.draw = function (doc) {
            if (thickness > 0) {
                doc.setLineWidth(thickness);
                doc.lines([[length, 0]], x, y);
            }
        }
    };

    function VerticalLine(
        x,
        y,
        length,
        thickness
    ) {
        this.draw = function (doc) {
            if (thickness > 0) {
                doc.setLineWidth(thickness);
                doc.lines([[0, length]], x, y);
            }
        }
    };


    this.save = function (file) {
        var doc = new jsPDF({
          orientation: 'landscape'
        });

        if (title) {
            doc.setFontSize(10);
            doc.text(title, 2, 5);
        }

        var page = {
            margin: 5,
            width: 297,
            height: 210
        };

        var table = {
            width: tableCalendar.width(),
            height: tableCalendar.height()
        };

        var cell = {
            width: (page.width - 2 * page.margin) / table.width,
            height: (page.height - 2 * page.margin) / table.height
        }

        var cells = tableCalendar.cells();
        for (var i = 0; i < cells.length; i++) {
            var x = page.margin + cells[i].x * cell.width;
            var y = page.margin + cells[i].y * cell.height;

            if (cells[i].color != Colors.WHITE) {
                var r = cells[i].color.r;
                var g = cells[i].color.g;
                var b = cells[i].color.b;

                doc.setFillColor(r, g, b);
                doc.rect(
                    x,
                    y,
                    cell.width,
                    cell.height,
                    'F'
                );
            }

            new HorisontalLine(
                x,
                y,
                cell.width,
                cells[i].top
            ).draw(doc);
            new VerticalLine(
                x,
                y,
                cell.height,
                cells[i].left
            ).draw(doc);
            new HorisontalLine(
                x,
                y + cell.height,
                cell.width,
                cells[i].bottom
            ).draw(doc);
            new VerticalLine(
                x + cell.width,
                y,
                cell.height,
                cells[i].right
            ).draw(doc);

            var text = cells[i].text
            var alignRight = text.align.h === Align.RIGHT;
            var alignBottom = text.align.v === Align.BOTTOM;
            var alignHCenter = text.align.h === Align.CENTER;
            var alignVCenter = text.align.v === Align.CENTER;
            doc.setFontSize(text.size);
            doc.text(
                text.content,
                x + text.shift.x + (alignRight ? cell.width : alignHCenter ? cell.width/2 : 0),
                y + text.shift.y + (alignBottom ? cell.height : alignVCenter ? cell.height/2 : 0),
                {},
                text.rotated ? 90 : 0,
                alignRight ? 'right' : alignHCenter ? 'center' : ''
            );
        }

        doc.save(file);
    }
};
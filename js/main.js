$(document).ready(function () {

    $("#year").bind('keyup input', function () {
        var leap = new Year($("#year").val()).isLeap();
        $("#leap").prop('checked', leap);
    });

    $("#make-pdf").click(function() {
        year = $("#year").val();
        new PdfCalendar(
            new TableCalendar(
                new Calendar(
                    new Year(year)
                )
                .addEvent(new Easter(true, true, true))
                .addEvent(new Event(21, 01, Colors.GREEN, 'Dzien Babci'))
                .addEvent(new Event(22, 01, Colors.GREEN, 'Dzien Dziadka'))
                .addEvent(new Event(14, 02, Colors.GREEN, 'Walentynki'))
                .addEvent(new Event(08, 03, Colors.GREEN, 'Dzien Kobiet'))
                .addEvent(new Event(26, 05, Colors.GREEN, 'Dzien Matki'))
                .addEvent(new Event(01, 06, Colors.GREEN, 'Dzien Dziecka'))
                .addEvent(new Event(23, 06, Colors.GREEN, 'Dzien Ojca'))
                .addEvent(new Event(19, 09, Colors.GREEN, 'Dzien Pirata'))
                .addEvent(new Event(06, 12, Colors.GREEN, 'Mikolajki'))
                .addEvent(new Event(25, 12, Colors.GREEN, 'Boze Narodzenie'))
                .addEvent(new Event(26, 12, Colors.GREEN, 'Boze Narodzenie'))

                .addEvent(new Event(01, 01, Colors.PEACH, 'Nowy Rok'))
                .addEvent(new Event(01, 06, Colors.PEACH, 'Trzech Kroli'))
                .addEvent(new Event(01, 05, Colors.PEACH, 'Swieto Pracy'))
                .addEvent(new Event(03, 05, Colors.PEACH, 'Swieto Konstytucji'))
                .addEvent(new Event(15, 08, Colors.PEACH, 'Swieto Wojska Polskiego'))
                .addEvent(new Event(01, 11, Colors.PEACH, 'Wszystkich Swietych'))
                .addEvent(new Event(11, 11, Colors.PEACH, 'Swieto Niepodleglosci'))
                .addEvent(new TimeShift())
            ),
            year
        ).save('calendar' + year + '.pdf');
    });
});
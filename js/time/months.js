function Month(
    num,
    en,
    pl
) {
    this.num = num;
    this.name = {
        en: en,
        pl: pl
    };
};

Months = {
    JANUARY:    new Month(0, 'January', 'Styczen'),
    FEBRUARY:   new Month(1, 'February', 'Luty'),
    MARCH:      new Month(2, 'March', 'Marzec'),
    APRIL:      new Month(3, 'April', 'Kwiecien'),
    MAY:        new Month(4, 'May', 'Maj'),
    JUNE:       new Month(5, 'June', 'Czerwiec'),
    JULY:       new Month(6, 'July', 'Lipiec'),
    AUGUST:     new Month(7, 'August', 'Sierpien'),
    SEPTEMBER:  new Month(8, 'September', 'Wrzesien'),
    OCTOBER:    new Month(9, 'October', 'Pazdziernik'),
    NOVEMBER:   new Month(10, 'November', 'Listopad'),
    DECEMBER:   new Month(11, 'December', 'Grudzien')
};

Months.order = [
    Months.JANUARY,
    Months.FEBRUARY,
    Months.MARCH,
    Months.APRIL,
    Months.MAY,
    Months.JUNE,
    Months.JULY,
    Months.AUGUST,
    Months.SEPTEMBER,
    Months.OCTOBER,
    Months.NOVEMBER,
    Months.DECEMBER
];
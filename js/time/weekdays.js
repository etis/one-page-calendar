function Weekday(
    num,
    en,
    pl
) {
    this.num = num;
    this.name = {
        en: en,
        pl: pl
    }
}

Weekdays = {
    MONDAY: new Weekday(0, 'Monday', 'Poniedzialek'),
    TUESDAY: new Weekday(1, 'Tuesday', 'Wtorek'),
    WEDNESDAY: new Weekday(2, 'Wednesday', 'Sroda'),
    THURSDAY: new Weekday(3, 'Thursday', 'Czwartek'),
    FRIDAY: new Weekday(4, 'Friday', 'Piatek'),
    SATURDAY: new Weekday(5, 'Saturday', 'Sobota'),
    SUNDAY: new Weekday(6, 'Sunday', 'Niedziela')
};

Week = [
    Weekdays.MONDAY,
    Weekdays.TUESDAY,
    Weekdays.WEDNESDAY,
    Weekdays.THURSDAY,
    Weekdays.FRIDAY,
    Weekdays.SATURDAY,
    Weekdays.SUNDAY
];

function WeekdayFromDate(
    num
) {
    return Week[(num + Week.length - 1) % Week.length];
}
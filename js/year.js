function Year(year) {
    this.year = year;

    this.weekday = function (day) {
        return WeekdayFromDate(new Date(year, 0, day).getDay());
    };

    this.month = function (day) {
        return Months.order[new Date(year, 0, day).getMonth()];
    }

    this.monthDay = function (day) {
        return new Date(year, 0, day).getDate();
    }

    var isLeap = function () {
        return new Date(year, 2, 0).getDate() === 29;
    };
    this.isLeap = isLeap;

    this.daysNum = function () {
        return isLeap() ? 366 : 365;
    };
}
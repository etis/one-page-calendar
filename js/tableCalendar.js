function TableCalendar(
    calendar
) {
    var DAY_CELL_HEIGHT = 6;
    var HEADER_LINES = 2;
    var SIDEBAR_COLUMNS = 1;
    var days = calendar.days();

    var startOffset = days[0].weekday.num;

    this.width = function () {
        return Math.ceil((startOffset + days.length) / Week.length)
                + SIDEBAR_COLUMNS; // left side legend
    }

    this.height = function () {
        return Week.length * DAY_CELL_HEIGHT
                + HEADER_LINES; // top legend
    }

    var dayPos = function (day) {
        return {
            x: Math.floor((startOffset + day) / Week.length)
                + SIDEBAR_COLUMNS,
            y: DAY_CELL_HEIGHT * ((startOffset + day) % Week.length)
                + HEADER_LINES
        }
    }

    this.cells = function () {
        var cells = [];

        for (var i = 0; i < Week.length; i++) {
            cells = cells.concat(multiCell(
                new Text(
                    Week[i].name.pl,
                    true,
                    Align.LEFT,
                    Align.BOTTOM,
                    10,
                    3.7,
                    -1.5
                ),
                0, DAY_CELL_HEIGHT * i + HEADER_LINES,
                1, DAY_CELL_HEIGHT,
                Colors.WHITE,
                0.6, 0.6, 0.6, 0.6
            ));
        }

        var weekNumCell = function (x, y, num) {
            return new Cell(
                new Text(
                    num.toString(),
                    false,
                    Align.RIGHT,
                    Align.TOP,
                    8,
                    -1.2,
                    3.2
                ),
                x,
                y,
                Colors.WHITE,
                0, 0, 0, 0
            );
        }

        var weekNum = 1;
        var monday = startOffset > 0 ? Week.length - startOffset : 0;
        for (var i = 0; i < Months.order.length; i++) {
            var start = dayPos(monday).x;
            var month = Months.order[i];
            while (monday < days.length && days[monday].month == month) {
                cells.push(weekNumCell(dayPos(monday).x, 1, weekNum));
                weekNum++;
                monday += Week.length;
            }
            var end = dayPos(monday).x;

            cells = cells.concat(multiCell(
                new Text(
                    month.name.pl,
                    false,
                    Align.LEFT,
                    Align.TOP,
                    10,
                    1,
                    4
                ),
                start,
                0,
                end - start,
                2,
                Colors.WHITE,
                0.6, 0.6, 0.6, 0.6
            ));
        }

        for (var i = 0; i < days.length; i++) {
            pos = dayPos(i)

            var walls = {
                left: (i - 7 < 0) || days[i - 7].month != days[i].month ? 0.6 : 0.3,
                right: (i + 7 >= days.length) || days[i + 7].month != days[i].month ? 0.6 : 0.3,
                top: days[i].weekday == Weekdays.MONDAY || (i - 1 < 0) || days[i - 1].month != days[i].month ? 0.6 : 0.3,
                bottom: days[i].weekday == Weekdays.SUNDAY || (i + 1 >= days.length) || days[i + 1].month != days[i].month ? 0.6 : 0.3
            }

            cells = cells.concat(multiCell(
                new Text(
                    days[i].monthDay.toString(),
                    false,
                    Align.RIGHT,
                    Align.TOP,
                    8,
                    -0.8,
                    3
                ),
                pos.x, pos.y,
                1, DAY_CELL_HEIGHT,
                days[i].color,
                walls.left,
                walls.right,
                walls.top,
                walls.bottom
            ));

            var labels = days[i].labels;
            for (var j = 0; j < labels.length; j++) {
                cells.push(new Cell(
                    new Text(
                        labels[j],
                        false,
                        Align.RIGHT,
                        Align.BOTTOM,
                        6,
                        -0.6,
                        -1 - 2 * j
                    ),
                    pos.x, pos.y + DAY_CELL_HEIGHT - 1,
                    Colors.WHITE,
                    0, 0, 0, 0
                ));
            }
        }
        return cells;
    }
}
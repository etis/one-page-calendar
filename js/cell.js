multiCell = function (
    text,
    x,
    y,
    w,
    h,
    color,
    left,
    right,
    top,
    bottom
) {
    var cells = [];

    var textPos = {
        x: text.align.h == Align.LEFT ? 0 : w - 1,
        y: text.align.v == Align.TOP ? 0 : h - 1
    }

    for (var i = 0; i < w; i++) {
        for (var j = 0; j < h; j++) {
            cells.push(new Cell(
                i === textPos.x && j === textPos.y ? text : EmptyText,
                x + i,
                y + j,
                color,
                i === 0 ? left : 0,
                i === w - 1 ? right : 0,
                j === 0 ? top : 0,
                j === h - 1 ? bottom : 0
            ));
        }
    }
    return cells;
}

function Cell(
    text,
    x,
    y,
    color,
    left,
    right,
    top,
    bottom
) {
    this.text = text;
    this.x = x;
    this.y = y;
    this.color = color;
    this.left = left;
    this.right = right;
    this.top = top;
    this.bottom = bottom;
}
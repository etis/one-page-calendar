function Easter(fatThursday, pentecost, corpusChristi) {

    this.days = function (yearObj) {
        var year = yearObj.year;

        // Instantiate the date object.
        var date = new Date;
        date.setHours(0, 0, 0, 0);
        date.setFullYear(year);

        // Find the golden number.
        var golden = year % 19;

        // Choose which version of the algorithm to use based on the given year.
        var algorythm = ( 2200 <= year && year <= 2299 ) ?
            ( ( 11 * golden ) + 4 ) % 30 :
            ( ( 11 * golden ) + 5 ) % 30;

        // Determine whether or not to compensate for the previous step.
        var compensate = ( ( algorythm === 0 ) || ( algorythm === 1 && golden > 10 ) ) ?
            ( algorythm + 1 ) :
            algorythm;

        // Use c first to find the month: April or March.
        var month = ( 1 <= compensate && compensate <= 19 ) ? 3 : 2;

        // Then use c to find the full moon after the northward equinox.
        var fullMoon = ( 50 - compensate ) % 31;

        // Mark the date of that full moon—the "Paschal" full moon.
        date.setMonth( month, fullMoon );

        // Count forward the number of days until the following Sunday.
        date.setMonth( month, fullMoon + ( 7 - date.getDay() ) );

        var friday = new Date(year, month, date.getDate() - 2);
        var saturday = new Date(year, month, date.getDate() - 1);
        var easter = new Date(year, month, date.getDate());
        var monday = new Date(year, month, date.getDate() + 1);

        var days = [];

        days.push(new EventDay(friday.getDate() - 1, friday.getMonth(), Colors.VIOLET, 'Wielki Piatek'));
        days.push(new EventDay(saturday.getDate() - 1, saturday.getMonth(), Colors.VIOLET, 'Wielka Sobota'));
        days.push(new EventDay(easter.getDate() - 1, easter.getMonth(), Colors.VIOLET, 'Wielkanoc'));
        days.push(new EventDay(monday.getDate() - 1, monday.getMonth(), Colors.VIOLET, 'Poniedzialek Wielkanocny'));

        if (fatThursday) {
            var fatThursdayDay = new Date(year, month, date.getDate() - 52);
            days.push(new EventDay(
                fatThursdayDay.getDate() - 1,
                fatThursdayDay.getMonth(),
                Colors.GREEN,
                'Tlusty Czwartek'
            ));
        }

        if (pentecost) {
            var pentecostDay = new Date(year, month, date.getDate() + 49);
            days.push(new EventDay(
                pentecostDay.getDate() - 1,
                pentecostDay.getMonth(),
                Colors.PEACH,
                'Zeslanie Ducha Swietego'
            ));
        }

        if (corpusChristi) {
            var corpusChristiDay = new Date(year, month, date.getDate() + 60);
            days.push(new EventDay(
                corpusChristiDay.getDate() - 1,
                corpusChristiDay.getMonth(),
                Colors.PEACH,
                'Boze Cialo'
            ));
        }

        return  days;
    }
};
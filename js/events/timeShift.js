function TimeShift() {
    this.days = function (year) {
        var lastDayOfOctober = new Date(year.year, Months.OCTOBER.num + 1, 0);
        var octoberSundayOffset = lastDayOfOctober.getDay();

        var lastDayOfMarch = new Date(year.year, Months.MARCH.num + 1, 0);
        var marchSundayOffset = lastDayOfMarch.getDay();

        return [
            new EventDay(lastDayOfMarch.getDate() - marchSundayOffset - 1, Months.MARCH.num, Colors.GREY, 'Czas +1h'),
            new EventDay(lastDayOfOctober.getDate() - octoberSundayOffset - 1, Months.OCTOBER.num, Colors.GREY, 'Czas -1h')
        ];
    }
}
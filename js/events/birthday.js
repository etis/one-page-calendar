function Birthday(
    day,
    month,
    whos
) {
    this.days = function() {
        return [
            new EventDay(day - 1, month - 1, Colors.BLUE, "Urodziny " + whos)
        ];
    }
}

function Nameday(
    day,
    month,
    whos
) {
    this.days = function() {
        return [
            new EventDay(day - 1, month - 1, Colors.BLUE, "Imieniny " + whos)
        ];
    }
}
function EventDay(
    day,
    month,
    color,
    text
) {
    this.day = day;
    this.month = month;
    this.color = color;
    this.text = text;
};

function Event(
    day,
    month,
    color,
    text
) {
    this.days = function () {
        return [
            new EventDay(day - 1, month - 1, color, text)
        ];
    }
};
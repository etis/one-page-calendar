function Calendar(
    year
) {
    var daysNum = year.daysNum();

    var days = [];
    var months = [];
    for (var i = 0; i < Months.order.length; i++) {
        months.push([]);
    }
    for (var i = 1; i < daysNum + 1; i++) {
        var weekday = year.weekday(i);
        var month = year.month(i);
        var monthDay = year.monthDay(i);
        var weekend = weekday == Weekdays.SATURDAY || weekday == Weekdays.SUNDAY;
        var day = new Day(
            weekend ? Colors.PEACH : Colors.WHITE,
            weekday,
            month,
            monthDay,
            []
        )
        months[month.num].push(day);
        days.push(day);
    }

    this.addEvent = function (event) {
        var days = event.days(year);
        for (var i = 0; i < days.length; i++) {
            var day = months[days[i].month][days[i].day];

            var initials = '';
            var words = days[i].text.split(' ');
            for (var j = 0; j < words.length; j++) {
                initials += words[j][0];
            }

            day.color = days[i].color;
            day.labels.push(initials.toLowerCase());
        }
        return this;
    }

    this.days = function () {
        return days;
    }
}
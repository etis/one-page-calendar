Colors = {
    WHITE:  {r: 255, g: 255, b: 255},
    BLACK:  {r: 000, g: 000, b: 000},
    PEACH:  {r: 255, g: 229, b: 180},
    BLUE:   {r: 137, g: 207, b: 240},
    GREEN:  {r: 208, g: 240, b: 192},
    VIOLET: {r: 195, g: 145, b: 209},
    GREY:   {r: 219, g: 227, b: 222}
};